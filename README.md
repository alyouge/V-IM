
### 声明：切勿使用本软件从事任何违法事宜，使用本软件产生的任何后果皆由使用者承担，本软件及开发者概不承担任何责任。

### 结构
>   1. v-im-pc 是聊天客户端，支持打包成exe 和 h5网页。
>   2. v-im-server 是服务端代码，集成了ruoyi的模块。
>   3. RuoYi-ui-vue3 是ruoyi管理系统的前端代码。
>   4. doc 下面有数据库等。
>   5. 1、2、3都是要启动的，务必先启动2
>   6. 开源版客户端连接 101.200.151.183 会报错，版本不一样，需要自己部署服务端，然后修改客户端的配置文件。

#### 使用部署文档
https://juejin.cn/user/3843548381983191/posts
### 企业版更新【2024年12月】


1. 新增接龙功能
2. 修改插件接入方式，无入侵方案
3. 优化部分代码，修复2个bug。

### 企业版更新【2024年10月】


1. PC端支持docx,xlsx,pdf，txt在线打开
2. 手机app端支持使用系统自动软件打开office，pdf附件
3. 支持图片右键复制为blob类型
4. 附件图标支持office类型图标
5. 支持ARM平台linux打包deb类型安装包



### 企业版测试

>   1. 企业版下载(windows)：https://v-im-oss.oss-cn-beijing.aliyuncs.com/auto-updates/V-IM-2.8.9-setup.exe
>   2. 企业版下载(linux AMD64，不同的linux可能是需要不同的包，必须在本平台下打包才能安装，比如AMD的CPU打包的在ARM架构的liunx下不能安装)：https://v-im-oss.oss-cn-beijing.aliyuncs.com/auto-updates/V-IM_2.8.1_amd64.deb
>   3. 企业版安卓APP https://v-im-oss.oss-cn-beijing.aliyuncs.com/app-updates/__UNI__87756B6__20250307121519.apk
>   4. linux ARM64下载   https://v-im-oss.oss-cn-beijing.aliyuncs.com/auto-updates/V-IM_2.8.4_arm64.deb
>   5. 网页版 http://101.200.151.183
### 企业版优势。
> 1. 多终端支持：PC(windows、linux、web) electron方案。
> 2. 手机（安卓、IOS、H5、小程序）,uni-app方案。
> 3. 上传支持两种方案(直接存服务器和minio)。
> 4. 私有云代码仓库永久更新，无加密部分，不依赖第三方。
> 5. 一对一技术支持。
> 6. bug修复优先级最高。
> 7. 支持付费定制化需求。
> 8. 功能更新频率高。
> 9. 集成成本最低的解决方案，前端技术路径就是Vue3技术栈，后端是Springboot3技术栈。

### 开源与企业版功能点对比（最终功能以上方测试版本为准，此功能对比仅供参考）
| 功能点                             | 开源版本-PC | 开源版本-Web | 企业版-Windows | 企业版-Linux | 企业版-Mac（放弃测试） | 企业版-Web | 企业版-安卓 | 企业版-IOS | 企业版-H5 | 企业版-小程序 |
|------------------------------------|-------------|--------------|-----------------|--------------|------------|------------|-------------|------------|-----------|---------------|
| **基础功能**                       |             |              |                 |              |            |            |             |            |           |               |
| 文本聊天                           | √           | √            | √               | √            | √          | √          | √           | √          | √         | √             |
| 聊天表情                           | √           | √            | √               | √            | √          | √          | √           | √          | √         | √             |
| 发送图片                           | √           | √            | √               | √            | √          | √          | √           | √          | √         | √             |
| 发送文件                           | √           | √            | √               | √            | √          | √          | √           |            | √         |               |
| 单聊/群聊                          | √           | √            | √               | √            | √          | √          | √           | √          | √         | √             |
| 离线消息/聊天记录                  | √           | √            | √               | √            | √          | √          | √           | √          | √         | √             |
| 断线重连/系统登录                  | √           | √            | √               | √            | √          | √          | √           | √          | √         | √             |
| **社交管理**                       |             |              |                 |              |            |            |             |            |           |               |
| 好友添加/群管理                    | √           | √            | √               | √            | √          | √          | √           | √          | √         | √             |
| 树状组织机构/消息转发              | √           | √            | √               | √            | √          | √          | √           | √          | √         | √             |
| 录音消息                           |             |              |                 |              |            |            | √           | √          |           |               |
| 好友审核/复杂群管理                |             |              | √               | √            | √          | √          | √           | √          | √         | √             |
| **高级功能**                       |             |              |                 |              |            |            |             |            |           |               |
| 消息收藏/消息@                     |             |              | √               | √            | √          | √          | √           | √          | √         | √             |
| 发送MP4视频                        |             |              | √               | √            | √          | √          | √           | √          |           | √             |
| 富文本输入框                       |             |              | √               | √            | √          | √          |             |            |           |               |
| 消息免打扰/置顶/语音提醒           |             |              | √               | √            | √          | √          | √           | √          | √         | √             |
| 自动更新                           |             |              | √               | √            | √          |            | √           |            |           |               |
| 音视频通话                         |             |              | √               | √            | √          |            | √           | √          |           |               |
| 拖动上传/粘贴上传                  |             |              | √               | √            | √          | √          |             |            |           |               |
| 消息多选操作/已读状态              |             |              | √               | √            | √          | √          | √           | √          | √         | √             |
| **企业版专属**                     |             |              |                 |              |            |            |             |            |           |               |
| 聊天记录存储（MongoDB + MinIO）    |             |              |                 |              |            |            |             |            |           |               |


#### 企业版咨询加微，源码微信联系（有偿）:备注v-im，并且附上点赞的gitee用户名！![](doc/wx.png)

### 企业版截图
![输入图片说明](v-im.png)
![消息列表/聊天](https://gitee.com/lele-666/V-IM/raw/master/doc/img/1.png)
![好友](https://gitee.com/lele-666/V-IM/raw/master/doc/img/2.png)
![组织](https://gitee.com/lele-666/V-IM/raw/master/doc/img/3.png)
![群组](https://gitee.com/lele-666/V-IM/raw/master/doc/img/4.png)
![添加好友](https://gitee.com/lele-666/V-IM/raw/master/doc/img/5.png)
![输入图片说明](doc/uniapp/m%20(1).jpg)
![输入图片说明](doc/uniapp/m%20(2).jpg)
![输入图片说明](doc/uniapp/m%20(3).jpg)
![输入图片说明](doc/uniapp/m%20(4).jpg)
![输入图片说明](doc/uniapp/m%20(5).jpg)
![输入图片说明](doc/uniapp/m%20(6).jpg)
![输入图片说明](doc/uniapp/m%20(7).jpgg)
![输入图片说明](doc/uniapp/m%20(8).jpg)
![输入图片说明](doc/uniapp/m%20(9).jpg)
![输入图片说明](doc/uniapp/m%20(10).jpg)
![输入图片说明](doc/uniapp/m%20(11).jpg)
![输入图片说明](doc/uniapp/m%20(12).jpg)
![输入图片说明](doc/uniapp/m%20(13).jpg)
![输入图片说明](doc/uniapp/m%20(14).jpg)

### 参考项目及技术
> 1. RuoYi-vue（https://gitee.com/y_project/RuoYi-Vue）
> 2. layIM（主要是聊天表情，文件处理方面）。
> 3. 使用SpringBoot、oauth2.0、t-io 开发后端服务。
> 4. vue3.0、element-plus、typescript开发前端。
> 5. 界面高仿微信。
> 6. 其他：使用 fetch 发送ajax 请求，支持跨域，electron 支持打包成为exe，也支持linux 和 mac。
> 7.  系统是在RuoYi-vue(https://gitee.com/y_project/RuoYi-Vue) 的基础上开发的，但是把数据库操作改成mybatis-plus,原先的是mybatis（如果你想完全迁移到RuoYi系统里面，可能还需要一定的工作量）。

### 交流授权
>  1. 如果您觉得好用，可以给点个star，或者给个捐赠。
>  2. 商用请购买企业版，功能更多。